package com.sliit.esad.health.web.model;

public class Appointment {
	
	private long sessionId;

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

}
