package com.sliit.esad.health.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sliit.esad.health.web.UserSession;
import com.sliit.esad.health.web.model.Appointment;
import com.sliit.esad.health.web.model.AppointmentFilter;
import com.sliit.esad.health.web.model.ConsultationSession;
import com.sliit.esad.health.web.service.HealthService;

@Controller
public class AppointmentController {

	@Autowired
	HealthService healthService;

	List<ConsultationSession> sessions = new ArrayList<ConsultationSession>();

	@RequestMapping(value = "appointment/search", method = RequestMethod.GET)
	public String searchAppointment(HttpServletRequest request, Model model) {

		UserSession user = (UserSession) request.getSession().getAttribute("scopedTarget.userSession");

		model.addAttribute("module", "appointment");
		AppointmentFilter filter = new AppointmentFilter();
		filter.setPatientEmail(user.getUser());
		model.addAttribute("filter", filter);
		return "appointment/search";
	}

	@RequestMapping(value = "appointment/search", method = RequestMethod.POST)
	public String searchAppointment(AppointmentFilter filter) {

		sessions = healthService.searchSessions(filter);
		return "redirect:/appointment/searchResults";
	}

	@RequestMapping(value = "appointment/searchResults", method = RequestMethod.GET)
	public String searchResults(HttpServletRequest request, Model model) {

		model.addAttribute("result", sessions);
		return "appointment/searchResults";
	}

	@RequestMapping(value = "appointment/create", method = RequestMethod.GET)
	public String createAppointment(HttpServletRequest request, Model model,
			@RequestParam(value = "sessionId", required = true) long sessionId) {

		Appointment app = new Appointment();
		app.setSessionId(sessionId);
		model.addAttribute("appointment", app);

		return "appointment/makeAppointment";
	}

	@RequestMapping(value = "appointment/create", method = RequestMethod.POST)
	public String createAppointment(Appointment appointment) {

		// TODO: Create logic goes here
		return "redirect:/appointment/search";
	}
}
