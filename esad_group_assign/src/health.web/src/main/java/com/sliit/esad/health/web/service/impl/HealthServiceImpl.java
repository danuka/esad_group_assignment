package com.sliit.esad.health.web.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sliit.esad.health.web.model.AppointmentFilter;
import com.sliit.esad.health.web.model.ConsultationSession;
import com.sliit.esad.health.web.model.Patient;
import com.sliit.esad.health.web.service.HealthService;

@Service
public class HealthServiceImpl implements HealthService {

	@Autowired
	Environment env;

	RestTemplate restTemplate;

	public HealthServiceImpl() {
		restTemplate = new RestTemplate();
	}

	private String patientBaseUrl() {
		return env.getProperty("patinetapi");
	}

	private String adminBaseUrl() {
		return env.getProperty("adminapi");
	}

	@Override
	public Patient authenticatePatient(String email) {

		ResponseEntity<Patient> rateResponse = restTemplate.exchange(patientBaseUrl() + "patient/autenticate/" + email,
				HttpMethod.GET, null, new ParameterizedTypeReference<Patient>() {
				});
		return rateResponse.getBody();
	}

	@Override
	public List<ConsultationSession> searchSessions(AppointmentFilter filter) {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<AppointmentFilter> requestEntity = new HttpEntity<>(filter, requestHeaders);

		ResponseEntity<List<ConsultationSession>> rateResponse = restTemplate.exchange(
				adminBaseUrl() + "admin/searchsessions", HttpMethod.POST, requestEntity,
				new ParameterizedTypeReference<List<ConsultationSession>>() {
				});
		return rateResponse.getBody();
	}

}
