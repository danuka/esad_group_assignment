
package com.sliit.esad.health.web.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sessionId",
    "sessionStartTime",
    "sessionEndTime",
    "maxAppointments",
    "consultant",
    "hospital"
})
public class ConsultationSession {

    @JsonProperty("sessionId")
    private Integer sessionId;
    @JsonProperty("sessionStartTime")
    private String sessionStartTime;
    @JsonProperty("sessionEndTime")
    private String sessionEndTime;
    @JsonProperty("maxAppointments")
    private Integer maxAppointments;
    @JsonProperty("consultant")
    private Consultant consultant;
    @JsonProperty("hospital")
    private Hospital hospital;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("sessionId")
    public Integer getSessionId() {
        return sessionId;
    }

    @JsonProperty("sessionId")
    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    @JsonProperty("sessionStartTime")
    public String getSessionStartTime() {
        return sessionStartTime;
    }

    @JsonProperty("sessionStartTime")
    public void setSessionStartTime(String sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    @JsonProperty("sessionEndTime")
    public String getSessionEndTime() {
        return sessionEndTime;
    }

    @JsonProperty("sessionEndTime")
    public void setSessionEndTime(String sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    @JsonProperty("maxAppointments")
    public Integer getMaxAppointments() {
        return maxAppointments;
    }

    @JsonProperty("maxAppointments")
    public void setMaxAppointments(Integer maxAppointments) {
        this.maxAppointments = maxAppointments;
    }

    @JsonProperty("consultant")
    public Consultant getConsultant() {
        return consultant;
    }

    @JsonProperty("consultant")
    public void setConsultant(Consultant consultant) {
        this.consultant = consultant;
    }

    @JsonProperty("hospital")
    public Hospital getHospital() {
        return hospital;
    }

    @JsonProperty("hospital")
    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
