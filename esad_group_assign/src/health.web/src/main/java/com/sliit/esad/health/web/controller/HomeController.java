package com.sliit.esad.health.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sliit.esad.health.web.UserSession;
import com.sliit.esad.health.web.model.Patient;
import com.sliit.esad.health.web.service.HealthService;

@Controller
public class HomeController {

	@Autowired
	HealthService healthService;

	@Autowired
	UserSession session;

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String home(HttpServletRequest request, Model model,
			@RequestParam(value = "message", required = false) String message) {
		session.setUser(null);
		Patient patient = new Patient();
		model.addAttribute("patient", patient);
		model.addAttribute("message", message);
		model.addAttribute("module", "home");
		return "home";
	}

	@RequestMapping(value = "home", method = RequestMethod.POST)
	public String createPerson(Patient patient) {

		Patient ret = healthService.authenticatePatient(patient.getEmail());

		if (ret != null && ret.getPassword().equals(patient.getPassword())) {

			session.setUser(patient.getEmail());
			return "redirect:/appointment/search";
		}

		return "redirect:/home?message=Failed";
	}
}
