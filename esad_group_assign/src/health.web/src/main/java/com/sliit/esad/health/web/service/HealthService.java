package com.sliit.esad.health.web.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sliit.esad.health.web.model.AppointmentFilter;
import com.sliit.esad.health.web.model.ConsultationSession;
import com.sliit.esad.health.web.model.Patient;

@Service
public interface HealthService {

	public Patient authenticatePatient(String email);

	public List<ConsultationSession> searchSessions(AppointmentFilter filter);
}
