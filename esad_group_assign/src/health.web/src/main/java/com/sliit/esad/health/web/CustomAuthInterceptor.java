package com.sliit.esad.health.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class CustomAuthInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest requestServlet, HttpServletResponse responseServlet, Object handler)
			throws Exception {

		UserSession user = (UserSession) requestServlet.getSession().getAttribute("scopedTarget.userSession");

		if (user.getUser() != null) {
			return true;
		}
		responseServlet.sendRedirect("/home?message=Failed");
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception exception) throws Exception {

	}
}
