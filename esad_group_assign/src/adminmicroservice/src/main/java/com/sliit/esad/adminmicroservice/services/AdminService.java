package com.sliit.esad.adminmicroservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sliit.esad.adminmicroservice.exceptions.InvalidSessionException;
import com.sliit.esad.adminmicroservice.model.Consultant;
import com.sliit.esad.adminmicroservice.model.ConsultationSession;
import com.sliit.esad.adminmicroservice.model.Hospital;
import com.sliit.esad.adminmicroservice.model.SearchFilter;

@Service
public interface AdminService {

	public Consultant createConsultant(Consultant consultant);

	public List<Consultant> getAllConsultants();

	public Hospital createHospital(Hospital hospital);

	public List<Hospital> getAllHospitals();

	public ConsultationSession createSession(ConsultationSession session) throws InvalidSessionException;

	public List<ConsultationSession> getAllSessions();
	
	public List<ConsultationSession> getSessionsBySearch(SearchFilter filter);

	public ConsultationSession getSessionById(long id);
}
