package com.sliit.esad.adminmicroservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sliit.esad.adminmicroservice.model.Hospital;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {

}
