package com.sliit.esad.adminmicroservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sliit.esad.adminmicroservice.model.Consultant;
import com.sliit.esad.adminmicroservice.model.ConsultationSession;

@Repository
public interface SessionRepository extends JpaRepository<ConsultationSession, Long> {

	public List<ConsultationSession> findByConsultant(Consultant consultant);

	@Query("from ConsultationSession s where s.consultant.name like :consName% and s.hospital.name like :hosName%")
	public List<ConsultationSession> findBySearch(@Param("consName") String consName, @Param("hosName") String hosName);
}
