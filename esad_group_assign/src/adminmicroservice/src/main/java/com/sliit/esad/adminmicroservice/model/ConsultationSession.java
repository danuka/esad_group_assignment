package com.sliit.esad.adminmicroservice.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ConsultationSession {

	@Id
	@GeneratedValue
	private long sessionId;
	private Date sessionStartTime;
	private Date sessionEndTime;
	private int maxAppointments;

	@ManyToOne
	private Consultant consultant;

	@ManyToOne
	private Hospital hospital;

	public long getSessionId() {
		return sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}

	public Date getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(Date sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public Date getSessionEndTime() {
		return sessionEndTime;
	}

	public void setSessionEndTime(Date sessionEndTime) {
		this.sessionEndTime = sessionEndTime;
	}

	public int getMaxAppointments() {
		return maxAppointments;
	}

	public void setMaxAppointments(int maxAppointments) {
		this.maxAppointments = maxAppointments;
	}

	public Consultant getConsultant() {
		return consultant;
	}

	public void setConsultant(Consultant consultant) {
		this.consultant = consultant;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}
}
