package com.sliit.esad.adminmicroservice.model;

public enum Speciality {

	PHYSICIAN, DERMATOLOGIST, RADIOLOGIST, ANESTHESIOLOGIST
}
