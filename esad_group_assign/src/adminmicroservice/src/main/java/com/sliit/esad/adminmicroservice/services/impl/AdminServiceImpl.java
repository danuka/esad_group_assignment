package com.sliit.esad.adminmicroservice.services.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sliit.esad.adminmicroservice.exceptions.InvalidSessionException;
import com.sliit.esad.adminmicroservice.model.Consultant;
import com.sliit.esad.adminmicroservice.model.ConsultationSession;
import com.sliit.esad.adminmicroservice.model.Hospital;
import com.sliit.esad.adminmicroservice.model.SearchFilter;
import com.sliit.esad.adminmicroservice.repository.ConsultantRepository;
import com.sliit.esad.adminmicroservice.repository.HospitalRepository;
import com.sliit.esad.adminmicroservice.repository.SessionRepository;
import com.sliit.esad.adminmicroservice.services.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	ConsultantRepository consultantRepository;

	@Autowired
	SessionRepository sessionRepository;

	@Autowired
	HospitalRepository hospitalRepository;

	@Override
	public Consultant createConsultant(Consultant consultant) {
		return consultantRepository.save(consultant);
	}

	@Override
	public List<Consultant> getAllConsultants() {
		return consultantRepository.findAll();
	}

	@Override
	public Hospital createHospital(Hospital hospital) {
		return hospitalRepository.save(hospital);
	}

	@Override
	public List<Hospital> getAllHospitals() {
		return hospitalRepository.findAll();
	}

	@Override
	public ConsultationSession createSession(ConsultationSession session) throws InvalidSessionException {

		Consultant cons = consultantRepository.findById(session.getConsultant().getId()).get();

		List<ConsultationSession> sessions = sessionRepository.findByConsultant(cons);

		Calendar now = Calendar.getInstance();
		now.setTime(session.getSessionStartTime());
		now.add(Calendar.MINUTE, 10 * session.getMaxAppointments());
		session.setSessionEndTime(now.getTime());

		for (ConsultationSession se : sessions) {
			if (chackStartDateRange(se, session.getSessionStartTime())
					&& chackEndDateRange(se, session.getSessionEndTime())) {
				throw new InvalidSessionException();
			}
		}

		return sessionRepository.save(session);
	}

	@Override
	public List<ConsultationSession> getAllSessions() {
		return sessionRepository.findAll();
	}

	@Override
	public ConsultationSession getSessionById(long id) {
		return sessionRepository.findById(id).get();
	}

	@Override
	public List<ConsultationSession> getSessionsBySearch(SearchFilter filter) {

		List<ConsultationSession> ret = sessionRepository.findBySearch(filter.getDoctorName(),
				filter.getHospitalName());

		List<ConsultationSession> finalList = new ArrayList<ConsultationSession>();

		for (ConsultationSession session : ret) {
			if (filter.getDate() != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				if (sdf.format(session.getSessionStartTime()).equals(sdf.format(filter.getDate()))) {
					finalList.add(session);
				}
			} else {
				finalList.add(session);
			}
		}

		return finalList;
	}

	private boolean chackStartDateRange(ConsultationSession se, Date startTime) {
		if ((se.getSessionStartTime().before(startTime) && se.getSessionEndTime().after(startTime))
				|| (se.getSessionStartTime().getTime() == startTime.getTime())
				|| (se.getSessionEndTime().getTime() == startTime.getTime())) {
			return true;
		}
		return false;
	}

	private boolean chackEndDateRange(ConsultationSession se, Date endTime) {
		if ((se.getSessionStartTime().before(endTime) && se.getSessionEndTime().after(endTime))
				|| (se.getSessionStartTime().getTime() == endTime.getTime())
				|| (se.getSessionEndTime().getTime() == endTime.getTime())) {
			return true;
		}
		return false;
	}

}
