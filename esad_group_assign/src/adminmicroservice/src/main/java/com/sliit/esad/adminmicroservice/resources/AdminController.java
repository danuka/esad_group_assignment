package com.sliit.esad.adminmicroservice.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sliit.esad.adminmicroservice.exceptions.InvalidSessionException;
import com.sliit.esad.adminmicroservice.model.Consultant;
import com.sliit.esad.adminmicroservice.model.ConsultationSession;
import com.sliit.esad.adminmicroservice.model.Hospital;
import com.sliit.esad.adminmicroservice.model.SearchFilter;
import com.sliit.esad.adminmicroservice.services.AdminService;

@RequestMapping("/admin")
@RestController
public class AdminController {

	@Autowired
	AdminService adminService;

	@RequestMapping(value = "/createconsultant", method = RequestMethod.POST)
	public ResponseEntity<Consultant> createConsultant(@RequestBody Consultant consultant) {

		Consultant res = adminService.createConsultant(consultant);
		return new ResponseEntity<>(res, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/getallconsultants", method = RequestMethod.GET)
	public ResponseEntity<List<Consultant>> getAllConsultants() {

		List<Consultant> res = adminService.getAllConsultants();
		return new ResponseEntity<>(res, HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/createhospital", method = RequestMethod.POST)
	public ResponseEntity<Hospital> createHospital(@RequestBody Hospital hospital) {

		Hospital res = adminService.createHospital(hospital);
		return new ResponseEntity<>(res, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/getallhospitals", method = RequestMethod.GET)
	public ResponseEntity<List<Hospital>> getAllHospitals() {

		List<Hospital> res = adminService.getAllHospitals();
		return new ResponseEntity<>(res, HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/createsession", method = RequestMethod.POST)
	public ResponseEntity<ConsultationSession> createSession(@RequestBody ConsultationSession session) {

		ConsultationSession res;
		try {
			res = adminService.createSession(session);
			return new ResponseEntity<>(res, HttpStatus.CREATED);
		} catch (InvalidSessionException e) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/getallsessions", method = RequestMethod.GET)
	public ResponseEntity<List<ConsultationSession>> getAllSessionss() {

		List<ConsultationSession> res = adminService.getAllSessions();
		return new ResponseEntity<>(res, HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/searchsessions", method = RequestMethod.POST)
	public ResponseEntity<List<ConsultationSession>> searchSessions(@RequestBody SearchFilter filter) {

		List<ConsultationSession> res = adminService.getSessionsBySearch(filter);
		return new ResponseEntity<>(res, HttpStatus.ACCEPTED);
	}
}