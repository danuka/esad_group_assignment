package com.sliit.esad.adminmicroservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sliit.esad.adminmicroservice.model.Consultant;

@Repository
public interface ConsultantRepository extends JpaRepository<Consultant, Long> {
	public List<Consultant> findByEmail(String email);
}
