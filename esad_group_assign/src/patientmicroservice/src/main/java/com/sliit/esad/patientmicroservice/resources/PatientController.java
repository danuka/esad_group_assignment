package com.sliit.esad.patientmicroservice.resources;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sliit.esad.patientmicroservice.exceptions.InvalidPatientException;
import com.sliit.esad.patientmicroservice.model.Patient;
import com.sliit.esad.patientmicroservice.services.PatientService;

@RequestMapping("/patient")
@RestController
public class PatientController {

	@Autowired
	private PatientService patientService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<Patient> createPatient(@RequestBody Patient patient) {
		try {
			Patient patientRes = patientService.createPatient(patient);
			return new ResponseEntity<>(patientRes, HttpStatus.OK);
		} catch (InvalidPatientException e) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/autenticate/{email}", method = RequestMethod.GET)
	public Patient authenticatePatient(@PathVariable("email") String email) throws ParseException {
		return patientService.getPatientByEmail(email);
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public List<Patient> authenticatePatient() throws ParseException {
		return patientService.getAllPatients();
	}
}
