package com.sliit.esad.patientmicroservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sliit.esad.patientmicroservice.model.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

	public Patient findById(long id);
	
	public List<Patient> findByEmail(String email);

}
