package com.sliit.esad.patientmicroservice.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sliit.esad.patientmicroservice.exceptions.InvalidPatientException;
import com.sliit.esad.patientmicroservice.model.Patient;
import com.sliit.esad.patientmicroservice.repository.PatientRepository;
import com.sliit.esad.patientmicroservice.services.PatientService;

@Service
public class PatientServiceImpl implements PatientService {

	@Autowired
	PatientRepository patientRepo;

	@Override
	public Patient getPatientByEmail(String email) {
		return patientRepo.findByEmail(email).get(0);
	}

	@Override
	public List<Patient> getAllPatients() {
		return patientRepo.findAll();
	}

	@Override
	public Patient createPatient(Patient patient) throws InvalidPatientException {

		List<Patient> exPatient = patientRepo.findByEmail(patient.getEmail());

		if (exPatient.isEmpty()) {
			return patientRepo.save(patient);
		}
		throw new InvalidPatientException("Validation error");
	}

}
