package com.sliit.esad.patientmicroservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sliit.esad.patientmicroservice.exceptions.InvalidPatientException;
import com.sliit.esad.patientmicroservice.model.Patient;

@Service
public interface PatientService {

	public Patient getPatientByEmail(String email);

	public List<Patient> getAllPatients();

	public Patient createPatient(Patient patient) throws InvalidPatientException;
}
