package com.sliit.esad.prescriptionmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrescriptionmicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrescriptionmicroserviceApplication.class, args);
	}

}
